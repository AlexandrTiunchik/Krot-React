import { combineReducers } from 'redux';

import userReducer from './Reducers/userReducer';
import dishReducer from './Reducers/dishReducer';
import orderReducer from './Reducers/orderReducer';
import basketReducer from './Reducers/basketReducer';
import newsReducer from './Reducers/newsReducer';
import commentReducer from './Reducers/commentReducer';
import menuReducer from './Reducers/menuReducer';

export default combineReducers({
  userReducer,
  dishReducer,
  orderReducer,
  basketReducer,
  menuReducer,
  newsReducer,
  commentReducer,
});
