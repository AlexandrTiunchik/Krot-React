import CommentApi from '../../Services/Api/commentApi';
import { SET_COMMENTS, SET_USER_COMMENT } from '../Types/commentTypes';

export const setComments = (comments: any) => ({
  type: SET_COMMENTS,
  payload: comments,
});

export const setUserComment = (comment: any) => ({
  type: SET_USER_COMMENT,
  payload: comment,
});

export const getComments = (dishId: string) => async dispatch => {
  try {
    const responseData: any = await new CommentApi().getComments(dishId);
    dispatch(setComments(responseData.data));
  } catch (err) {
    throw err;
  }
};

export const getUserComment = (params: any) => async dispatch => {
  try {
    const responseData: any = await new CommentApi().getUserComment(params);
    dispatch(setUserComment(responseData.data));
  } catch (err) {
    throw err;
  }
};

export const deleteComment = (dishId: string) => async dispatch => {
  try {
    await new CommentApi().deleteComment(dishId);
    dispatch(getUserComment({ dishId }));
    dispatch(getComments(dishId));
  } catch (err) {
    throw err;
  }
};

export const createComment = (comment: any, dishId: string) => async dispatch => {
  try {
    await new CommentApi().createComment(comment, dishId);
    dispatch(getComments(dishId));
    dispatch(getUserComment({ dishId }));
  } catch (err) {
    throw err;
  }
};
