import DishApi from '../../Services/Api/dishApi';
import { SET_DISHES, SET_POPULAR_DISHES } from '../Types/dishTypes';
import IDish from '../../Interfaces/IDish';

export const getAllDishes = () => async dispatch => {
  try {
    const responseData: any = await new DishApi().getAllDish();
    dispatch(setDishes(responseData.data));
  } catch (err) {
    throw err;
  }
};

export const deleteDish = (id: string) => async dispatch => {
  try {
    await new DishApi().deleteDish(id);
    dispatch(getAllDishes());
  } catch (err) {
    throw err;
  }
};

export const getPopularDishes = () => async dispatch => {
  try {
    const responseData: any = await new DishApi().getDishes();
    dispatch(setPopularDishes(responseData.data));
  } catch (err) {
    throw err;
  }
};

export const getDish = (id: string) => async () => {
  try {
    return await new DishApi().getSingleDish(id);
  } catch (err) {
    throw err;
  }
};

export const updateDish = (data: IDish, id: string) => async dispatch => {
  try {
    await new DishApi().updateDish(id, data);
    dispatch(getAllDishes());
  } catch (err) {
    throw err;
  }
};

export const addDish = (data: IDish) => async dispatch => {
  try {
    await new DishApi().addDish(data);
    dispatch(getAllDishes());
  } catch (err) {
    throw err;
  }
};

export const setDishes = (dishes: IDish[]) => ({
  type: SET_DISHES,
  payload: dishes,
});

export const setPopularDishes = (dishes: any) => ({
  type: SET_POPULAR_DISHES,
  payload: dishes,
});
