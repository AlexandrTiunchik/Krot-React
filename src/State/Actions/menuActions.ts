import MenuApi from '../../Services/Api/menuApi';
import { SET_MENU } from '../Types/menuTypes';


export const setDishes = (dishes: any) => ({
  type: SET_MENU,
  payload: dishes,
});


export const getMenu = (params: any) => async dispatch => {
  try {
    const responseData: any = await new MenuApi().getMenu(params);
    dispatch(setDishes(responseData.data));
  } catch (err) {
    throw err;
  }
};
