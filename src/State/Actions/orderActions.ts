import OrderApi from '../../Services/Api/orderApi';
import { IOrder } from '../../Interfaces/IOrder';
import { SET_ORDERS, SET_USER_ORDERS } from '../Types/orderTypes';

export const getAllOrders = () => async dispatch => {
  try {
    const responseData: any = await new OrderApi().getAllOrders();
    dispatch(setOrders(responseData.data));
  } catch (err) {
    throw err;
  }
};

export const addOrder = (order: IOrder) => async (dispatch) => {
  try {
    await new OrderApi().addOrder(order);
  } catch (err) {
    throw err;
  }
};

export const addOrderFromBasket = () => async (dispatch) => {
  try {
    await new OrderApi().addOrder();
  } catch (err) {
    throw err;
  }
};


export const getUserOrders = (id: string) => async dispatch => {
  try {
    const responseData: any = await new OrderApi().getUserOrder(id);
    dispatch(setUserOrders(responseData.data));
  } catch (err) {
    throw err;
  }
};

export const deleteOrder = (id: string) => async dispatch => {
  try {
    await new OrderApi().deleteOrder(id);
    dispatch(getAllOrders());
  } catch (err) {
    throw err;
  }
};

export const updateOrder = (id: string, data: any) => async dispatch => {
  try {
    await new OrderApi().updateOrder(id, data);
    dispatch(getAllOrders());
  } catch (err) {
    throw err;
  }
};

export const setUserOrders = (orders: IOrder[]) => ({
  type: SET_USER_ORDERS,
  payload: orders,
});

export const setOrders = (orders: IOrder[]) => ({
  type: SET_ORDERS,
  payload: orders,
});
