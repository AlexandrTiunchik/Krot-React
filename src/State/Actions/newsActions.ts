import NewsApi from '../../Services/Api/newsApi';
import { SET_NEWS } from '../Types/newsTypes';


export const setNews = (news: []) => ({
  type: SET_NEWS,
  payload: news,
});

export const getNews = (params?: any) => async dispatch => {
  try {
    const responseData: any = await new NewsApi().getNews(params);
    dispatch(setNews(responseData.data));
  } catch (err) {
    throw err;
  }
};

export const deleteNews = (id: string) => async dispatch => {
  try {
    await new NewsApi().deleteNews(id);
    dispatch(getNews());
  } catch (err) {
    throw err;
  }
};

export const updateNews = (id: string, data: any) => async dispatch => {
  try {
    await new NewsApi().updateNews(id, data);
    dispatch(getNews());
  } catch (err) {
    throw err;
  }
};

export const addNews = (news: any) => async () => {
  try {
    await new NewsApi().addNews(news);
    getNews();
  } catch (err) {
    throw err;
  }
};
