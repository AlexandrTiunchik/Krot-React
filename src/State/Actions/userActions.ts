import axios from 'axios';

import LoginRequestModel from '../../Pages/LoginPage/LoginRequestModel';
import UserApi from '../../Services/Api/userApi';
import LocalStorageUtil from '../../Utils/LocalStorageUtil';
import { LOCAL_STORAGE } from '../../Constants/application';
import { IInitialUserState, IUserProfile } from '../Reducers/userReducer';
import { SET_USER, SET_USER_PROFILE } from '../Types/userTypes';
import RegistrationRequestModel from '../../Pages/RegistrationPage/RegistrationRequestModel';
import { ROUTE_PATH } from '../../Constants/router';

export const loginUserAction = (data: LoginRequestModel, routeHistory: any) => async dispatch => {
  try {
    const responseData: any = await new UserApi().login(data);
    LocalStorageUtil.set(LOCAL_STORAGE.TOKEN_NAME, responseData.data.token);
    dispatch(getUser());
    //routeHistory.push(ROUTE_PATH.MAIN_PAGE);
  } catch (err) {
    throw err;
  }
};

export const registrationUser = (user: RegistrationRequestModel, routerHistory: any) =>
  async () => {
    try {
      await new UserApi().registration(user);
      routerHistory.push(ROUTE_PATH.LOGIN);
    } catch (err) {
      throw err;
    }
};

export const getUser = (routerHistory?: any) => async dispatch => {
  try {
    axios.defaults.headers.common.Authorization = `Bearer ${LocalStorageUtil.get(LOCAL_STORAGE.TOKEN_NAME)}`;
    const responseData: any = await new UserApi().getUser();
    dispatch(setUser(responseData.data));
  } catch (err) {
    console.log(1)
    //dispatch(logoutUser(routerHistory));
  }
};

export const getUserProfileInfo = () => async dispatch => {
  try {
    const responseData: any = await new UserApi().getProfileInfo();
    const profile: IUserProfile = {
      name: responseData.data.name || '',
      surname: responseData.data.surname || '',
      patronymic: responseData.data.patronymic || '',
      telephone: responseData.data.telephone || '',
      city: responseData.data.city || '',
      address: responseData.data.address || '',
    };
    dispatch(setUserProfile(profile));
    return profile;
  } catch (err) {
    throw err;
  }
};

export const logoutUser = (routeHistory: any) => dispatch => {
  try {
    LocalStorageUtil.remove(LOCAL_STORAGE.TOKEN_NAME);
    dispatch(setUserProfile(null));
    dispatch({ type: SET_USER, payload: { role: 'guest' } });
    delete axios.defaults.headers.common.Authorization;
    routeHistory.push(ROUTE_PATH.REDIRECT_ROUTE);
  } catch (err) {
    throw err;
  }
};

export const updateProfile = userProfile => async dispatch => {
  try {
    const profile: IUserProfile = {
      name: userProfile.name,
      surname: userProfile.surname,
      patronymic: userProfile.patronymic,
      telephone: userProfile.telephone,
      city: userProfile.city,
      address: userProfile.address,
    };
    const responseData: any = await new UserApi().updateProfile(profile);
  } catch (err) {
    throw err;
  }
};

export const setUserProfile = (userProfile: any) => ({
  type: SET_USER_PROFILE,
  payload: userProfile,
});

export const setUser = (tokenPayload, token?: string) => {
  const userData: IInitialUserState = {
    token: token || null,
    id: tokenPayload.id,
    loginName: tokenPayload.loginName,
    role: tokenPayload.role,
  };
  return {
    type: SET_USER,
    payload: userData,
  };
};
