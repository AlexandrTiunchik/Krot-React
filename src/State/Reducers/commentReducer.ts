import { SET_COMMENTS, SET_USER_COMMENT } from '../Types/commentTypes';

export interface IInitialCommentsState {
  comments: [],
  userComment: []
}

const initialState: IInitialCommentsState = {
  comments: [],
  userComment: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_COMMENTS:
      return {
        ...state,
        comments: action.payload,
      };

    case SET_USER_COMMENT:
      return {
        ...state,
        userComment: action.payload,
      };

    default:
      return state;
  }
}
