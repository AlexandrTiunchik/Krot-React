import { SET_DISHES, SET_POPULAR_DISHES } from '../Types/dishTypes';
import IDish from '../../Interfaces/IDish';

export interface IInitialDishState {
  dishes: IDish[],
  popularDishes: IDish[],
}

const initialState: IInitialDishState = {
  dishes: [],
  popularDishes: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_DISHES:
      return {
        ...state,
        dishes: action.payload,
      };
    case SET_POPULAR_DISHES:
      return {
        ...state,
        popularDishes: action.payload,
      };

    default:
      return state;
  }
}
