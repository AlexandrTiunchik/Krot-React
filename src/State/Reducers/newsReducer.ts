import { SET_NEWS } from '../Types/newsTypes';

export interface IInitialNewsState {
  news: [],
}

const initialState: IInitialNewsState = {
  news: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_NEWS:
      return {
        ...state,
        news: action.payload,
      };
    default:
      return state;
  }
}
