import {SET_ORDERS} from "../Types/orderTypes";

export interface IInitialOrderState {
    orders: [];
}

const initialState: IInitialOrderState = {
    orders: []
};

export default function (state = initialState, actions) {
    switch (actions.type) {
        case SET_ORDERS:
            return {
                ...state,
                orders: actions.payload
            };

        default:
            return state;
    }
}
