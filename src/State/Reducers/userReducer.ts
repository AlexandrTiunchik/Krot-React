import { SET_USER, SET_USER_PROFILE } from '../Types/userTypes';
import { SET_USER_ORDERS } from '../Types/orderTypes';
import { IOrder } from '../../Interfaces/IOrder';

export interface IInitialUserState {
  token: string | null;
  id: string | null;
  loginName: string | null;
  role: string;
  profile?: IUserProfile | null;
  orders?: IOrder[] | null;
}

export interface IUserProfile {
  name?: string;
  surname?: string;
  patronymic?: string;
  telephone?: string;
  city?: string;
  address?: string;
}

const initialState: IInitialUserState = {
  token: null,
  id: null,
  loginName: null,
  role: 'guest',
  profile: {
    name: '',
    surname: '',
    patronymic: '',
    telephone: '',
    city: '',
    address: '',
  },
  orders: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        ...action.payload,
      };

    case SET_USER_PROFILE: {
      return {
        ...state,
        profile: action.payload,
      };
    }

    case SET_USER_ORDERS:
      return {
        ...state,
        orders: action.payload,
      };

    default:
      return state;
  }
}
