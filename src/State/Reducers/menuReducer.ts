import IDish from '../../Interfaces/IDish';
import { SET_MENU } from '../Types/menuTypes';

export interface IInitialMenuState {
  dishes: IDish[],
}

const initialState: IInitialMenuState = {
  dishes: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_MENU:
      return {
        ...state,
        dishes: action.payload,
      };
    default:
      return state;
  }
}
