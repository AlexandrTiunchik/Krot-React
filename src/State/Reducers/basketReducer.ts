import { SET_BASKET } from '../Types/basketTypes';

export interface IInitialBasketState {
  basket: any;
}

const initialState: IInitialBasketState = {
  basket: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_BASKET:
      return {
        ...state,
        basket: action.payload,
      };

    default:
      return state;
  }
}
