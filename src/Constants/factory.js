export const FACTORY = [
    {
      id: 0,
        lat: 53.644244,
        lng: 23.971850,
        title: 'Гродненская ГЭС',
      url: 'http://belnipi.by/%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D1%8B/%D0%B2-%D0%B1%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D0%B8/%D0%B3%D1%80%D0%BE%D0%B4%D0%BD%D0%B5%D0%BD%D1%81%D0%BA%D0%B0%D1%8F-%D0%B3%D1%8D%D1%81/'
    },
    {
      id: 1,
        lat: 52.455742,
        lng: 25.181098,
        title: 'Берёзовская ГРЭС',
      url: 'https://brestenergo.by/%D0%91%D0%B5%D1%80%D0%B5%D0%B7%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F_%D0%93%D0%A0%D0%AD%D0%A1'
    },
    {
      id: 2,
        lat: 53.872730,
        lng: 27.400796,
        title: 'Минская ТЭЦ-4',
      url: 'https://minskenergo.by/filialy/minskaya-tets-4/'
    },
    {
      id: 3,
        lat: 54.755008,
        lng: 26.091593,
        title: 'Белорусская атомная электростанция',
      url: 'http://belaes.by/ru/'
    },
    {
      id: 4,
        lat: 53.610754,
        lng: 27.947353,
        title: 'Минская ТЭЦ-5',
      url: 'http://tec5.by/'
    },
    {
      id: 5,
        lat: 55.524490,
        lng: 28.559773,
        title: 'Новополоцкая ТЭЦ',
      url: 'http://www.novopolotsk.by/content/view/3281/50/'
    },
    {
      id: 6,
        lat: 55.432489,
        lng: 28.946200,
        title: 'Полоцкая ГЭС',
      url: 'https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D0%BB%D0%BE%D1%86%D0%BA%D0%B0%D1%8F_%D0%93%D0%AD%D0%A1'
    },
    {
      id: 7,
        lat: 55.243268,
        lng: 30.163387,
        title: 'Витебская ГЭС',
      url: ''
    },
    {
      id: 8,
        lat: 54.676092,
        lng: 29.135967,
        title: 'Лукомльская ГРЭС',
      url: 'http://belnipi.by/%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D1%8B/%D0%B2-%D0%B1%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D0%B8/%D0%BB%D1%83%D0%BA%D0%BE%D0%BC%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F-%D0%B3%D1%80%D1%8D%D1%81/'
    },
    {
      id: 9,
        lat: 51.905763,
        lng: 29.312782,
        title: 'Мозырская ТЭЦ',
      url: 'http://www.gomelenergo.by/index.php?option=com_content&view=category&id=20&lang=ru'
    },
    {
      id: 10,
        lat: 52.450192,
        lng: 30.816908,
        title: 'Гомельская ТЭЦ-2',
      url: 'http://belnipi.by/%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D1%8B/%D0%B2-%D0%B1%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D0%B8/%D0%B3%D0%BE%D0%BC%D0%B5%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F-%D1%82%D1%8D%D1%86-2/'
    },
];

