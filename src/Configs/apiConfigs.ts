export const API_BASE_URL = !process.env.NODE_ENV
    || process.env.NODE_ENV === 'development' ? 'https://krot-nodejs.herokuapp.com' : 'https://krot-nodejs.herokuapp.com';
