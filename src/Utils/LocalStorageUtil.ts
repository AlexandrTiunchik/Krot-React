export default class LocalStorageUtil {
  public static set(key: string, value: any): void {
    localStorage.setItem(key, value);
  }

  public static remove(key: string): void {
    localStorage.removeItem(key);
  }

  public static get(key: string): any {
    return localStorage.getItem(key);
  }
}
