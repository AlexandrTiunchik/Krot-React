import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { ROUTE_PATH } from '../Constants/router';

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  const renderComponent = () => (rest.role.includes(rest.currentRole)
    ? <Component />
    : <Redirect to={ROUTE_PATH.REDIRECT_ROUTE} />);

  return (
    <Route exact={rest.exact} path={rest.path} render={renderComponent} />
  );
};
