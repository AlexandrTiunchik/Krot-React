import React from 'react';
import styles from './Styles/ErrorMessage.module.css';

export default function ErrorMessage(props) {
  return (
    <div className={styles.error}>
      {props.children}
    </div>
  );
}
