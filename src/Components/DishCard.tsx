import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';

import AdapterLink from './AdapterLink';
import { ROUTE_PATH } from '../Constants/router';

const useStyles = makeStyles({
  card: {
    maxWidth: 270,
  },
});

export default function ImgMediaCard(props: any) {
  const classes = useStyles();

  return (
    <Card className={`${classes.card} d-flex justify-content-between flex-column`}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="140"
          image={props.dish.image}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {props.dish.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {
              props.text
                ? props.dish.description.substring(0, 100) + '...'
                : null
            }
          </Typography>
        </CardContent>
        {props.dish.rating
          ? (
            <Rating
              className="ml-1"
              disabled
              value={props.dish.rating}
              name="rating"
            />
          )
          : null}
      </CardActionArea>
      <CardActions>
        <b>
          {props.dish.price}
        </b>
        <Button
          size="small"
          color="primary"
          component={AdapterLink}
          to={`${ROUTE_PATH.DISH}/${props.dish._id}`}
        >
          Прочитать
        </Button>
      </CardActions>
    </Card>
  );
}
