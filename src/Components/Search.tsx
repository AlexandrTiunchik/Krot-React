import React, {createRef, useState} from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import { InputBase } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import {withRouter} from "react-router";
import {ROUTE_PATH} from "../Constants/router";

const useStyles = makeStyles((theme) => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    cursor: 'pointer',
    width: theme.spacing(7),
    position: 'absolute',
    height: '100% !important',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

function Search(props) {
  const classes = useStyles();

  const [state, setState] = useState('');

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon onClick={() => console.log(props)}/>
      </div>
      <InputBase
        onChange={(event) => setState(event.target.value)}
        onKeyDown={(event) => {
          event.keyCode === 13 && props.history.push(`${ROUTE_PATH.SEARCH}?search=${state}`);
        }}
        placeholder="Поиск…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
      />
    </div>
  );
}

export default withRouter(Search);
