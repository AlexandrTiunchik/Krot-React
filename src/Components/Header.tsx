import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { Toolbar, Typography } from '@material-ui/core';
import { Link, withRouter } from 'react-router-dom';

import HeaderHOC from '../HOC/HeaderHOC';
import styles from './Styles/Header.module.css';
import { ROUTE_PATH } from '../Constants/router';
import Search from './Search';
import Menu from '../Pages/MainPage/Components/Menu/Menu';

function Header(props) {
  return (
    <div>
      <AppBar color="inherit" position="static">
        <Toolbar className={styles.header}>
          <div className={styles.searchBar}>
            <Typography variant="h6">
              <Link className={styles.logo} to={ROUTE_PATH.MAIN_PAGE}>Мир энергосбережения</Link>
            </Typography>
            <Search />
            <Menu />
          </div>
          <div className={styles['button-list']}>
              {props.rightPanel}
              {props.logout}
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withRouter(HeaderHOC(Header));
