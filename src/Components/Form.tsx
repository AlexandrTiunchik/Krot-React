import React from 'react';
import { ValidatorForm } from 'react-material-ui-form-validator';
import styles from './Styles/Form.module.css';

export default function Form(props: any) {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    props.setState({
      ...props.state,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <ValidatorForm
        onSubmit={props.onSubmit}
        onChange={handleChange}
        className={styles.form}
    >
        {props.children}
    </ValidatorForm>
  );
}
