import React from 'react';
import {MAP_INFO} from "../../Constants/mapInfo";

export default function MapInfoPage(props) {
  const { id } = props.match.params;

  return (
    <div className='container'>
      <div className='text-center mt-5'>
        <img src={MAP_INFO[id].image}/>
      </div>
      <h1 className='text-center'>{MAP_INFO[id].title}</h1>
      <p>{MAP_INFO[id].text}</p>
    </div>
  );
}
