import React, { useState } from 'react';
import { TextValidator } from 'react-material-ui-form-validator';

import { Button } from '@material-ui/core';
import Form from '../../../Components/Form';

export default function DishForm(props) {
  const [state, setState] = useState({
    title: '',
    image: '',
    description: '',
    price: '',
    ingredients: '',
    category: '',
    ...props.state,
  });

  const onSubmit = async () => {
    await props.action(state, state._id);
  };

  return (
    <div>

      <Form
        state={state}
        setState={setState}
        onSubmit={onSubmit}
      >
        <TextValidator
          label="Название"
          name="title"
          value={state.title}
        />

        <TextValidator
          label="Картинка"
          name="image"
          value={state.image}
        />

        <TextValidator
          label="Описание"
          name="description"
          value={state.description}
        />
        <Button type="submit">Отправить</Button>
      </Form>

    </div>
  );
}
