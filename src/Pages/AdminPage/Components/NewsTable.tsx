import React from 'react';
import MaterialTable from 'material-table';
import { Button } from '@material-ui/core';

import styles from '../AdminPage.module.css';

const tableColumns: any = [
  { title: 'Заголовок', field: 'title' },
  { title: 'Текст', field: 'text' },
  { title: 'Дата', field: 'date' },
];

function NewsTable(props: any) {
  const tableActions: any = [
    {
      icon: 'edit',
      tooltip: 'Редактировать',
      onClick: (event, rowData) => {
        props.onOpenDialog(true, 'updateNews', rowData);
      },
    },
  ];

  const tableToolBar: any = {
    Toolbar: () => (
      <div className="p-2">
        <div className={`${styles['table-name']} rounded`}>
          Новости на главной странице
        </div>
        <Button
          variant="outlined"
          onClick={() => {
            props.onOpenDialog(true, 'addNews');
          }}
        >
          Добавить
        </Button>
      </div>
    ),
  };

  const tableEditable: any = {
    isDeletable: () => true,
    onRowDelete: async (data: any) => {
      await props.deleteNews(data._id);
    },
  };

  return (
    <div
      className="mb-3"
      style={{ maxWidth: '100%' }}
    >
      <MaterialTable
        columns={tableColumns}
        data={props.news}
        title="Dish"
        actions={tableActions}
        components={tableToolBar}
        editable={tableEditable}
      />
    </div>
  );
}

export default NewsTable;
