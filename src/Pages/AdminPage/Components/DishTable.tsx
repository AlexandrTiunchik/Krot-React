import React from 'react';
import MaterialTable from 'material-table';
import { Button } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

import { ROUTE_PATH } from '../../../Constants/router';
import styles from '../AdminPage.module.css';

const tableColumns: any = [
  { title: 'Заголовок', field: 'title' },
  { title: 'Дата', field: 'date' },
];

function DishTable(props: any) {
  const tableActions: any = [
    {
      icon: 'edit',
      tooltip: 'редактировать новость',
      onClick: (event, rowData) => {
        props.onOpenDialog(true, 'updateDish', rowData);
      },
    },
    {
      icon: 'info',
      tooltip: 'Открыть новость',
      onClick: (event, rowData) => {
        props.history.push(`${ROUTE_PATH.DISH}/${rowData._id}`);
      },
    },
  ];

  const tableToolBar: any = {
    Toolbar: () => (
      <div className="p-2">
        <div className={`${styles['table-name']} rounded`}>
          Новости
        </div>
        <Button
          variant="outlined"
          onClick={() => {
          props.onOpenDialog(true, 'addDish');
          }}
        >
        Добавить
        </Button>
      </div>
    ),
  };

  const tableEditable: any = {
    isDeletable: () => true,
    onRowDelete: async (data: any) => {
      await props.deleteDish(data._id);
    },
  };

  return (
    <div className="mb-3" style={{ maxWidth: '100%' }}>
      <MaterialTable
        columns={tableColumns}
        data={props.dishes}
        title="Новость"
        actions={tableActions}
        components={tableToolBar}
        editable={tableEditable}
      />
    </div>
  );
}

export default withRouter(DishTable);
