import React, { useState } from 'react';
import { TextValidator } from 'react-material-ui-form-validator';

import { Button } from '@material-ui/core';
import Form from '../../../Components/Form';

export default function NewsForm(props: any) {
  const [state, setState] = useState({
    title: '',
    image: '',
    text: '',
    link: '',
    ...props.state,
  });

  const onSubmit = async () => {
    await props.action(state._id, state);
  };

  return (
    <div>

      <Form
        state={state}
        setState={setState}
        onSubmit={onSubmit}
      >
        <TextValidator
          label="Название"
          name="title"
          value={state.title}
        />

        <TextValidator
          label="Ссылка"
          name="link"
          value={state.link}
        />

        <TextValidator
          label="Картинка"
          name="image"
          value={state.image}
        />

        <TextValidator
          label="Текст"
          name="text"
          value={state.text}
        />

        <Button type="submit">Сохранить</Button>
      </Form>

    </div>
  );
}
