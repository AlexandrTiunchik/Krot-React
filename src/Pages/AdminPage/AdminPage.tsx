import React, { useState } from 'react';
import { connect } from 'react-redux';
import useAsyncEffect from 'use-async-effect';

import DishTable from './Components/DishTable';
import ResponsiveDialog from '../../Components/ResponsiveDialog';
import DishForm from './Components/DishForm';
import {
  addDish, deleteDish, getAllDishes, updateDish,
} from '../../State/Actions/dishActions';
import styles from './AdminPage.module.css';
import NewsTable from './Components/NewsTable';
import {
  addNews, deleteNews, getNews, updateNews,
} from '../../State/Actions/newsActions';
import NewsForm from './Components/NewsForm';

function AdminPage(props: any) {
  const [state, setState] = useState({
    isUpdateDialog: false,
    dialogType: '',
    isAddDialog: false,
    data: {
      title: '',
    },
  });

  const dialogSettings = {
    updateDish: (
      <DishForm
        state={state.data}
        setState={setState}
        action={props.updateDish}
      />
    ),
    addDish: (
      <DishForm
        state={state.data}
        setState={setState}
        action={props.addDish}
      />
    ),
    addNews: (
      <NewsForm
        state={state.data}
        setState={setState}
        action={props.addNews}
      />
    ),
    updateNews: (
      <NewsForm
        state={state.data}
        setState={setState}
        action={props.updateNews}
      />
    ),
  };

  useAsyncEffect(async () => {
    await props.getAllDishes();
    await props.getNews();
  }, []);

  const onOpenDialog = (flag: boolean, dialogType: string, data?: any) => {
    setState({
      ...state,
      isUpdateDialog: flag,
      dialogType,
      data: data,
    });
  };

  return (
    <div className={`container ${styles['admin-container']}`}>
      <ResponsiveDialog
        title={state.dialogType}
        onOpenDialog={onOpenDialog}
        isOpen={state.isUpdateDialog}
      >
        {dialogSettings[state.dialogType]}
      </ResponsiveDialog>
      <DishTable
        dishes={props.dishReducer.dishes}
        addDish={props.addDish}
        deleteDish={props.deleteDish}
        onOpenDialog={onOpenDialog}
      />
      <NewsTable
        news={props.newsReducer.news}
        addNews={props.addNews}
        deleteNews={props.deleteNews}
        onOpenDialog={onOpenDialog}
      />
    </div>
  );
}

const mapStateToProps = state => ({
  dishReducer: state.dishReducer,
  newsReducer: state.newsReducer,
});

const actions = {
  getAllDishes,
  updateDish,
  deleteDish,
  addDish,
  getNews,
  deleteNews,
  addNews,
  updateNews,
};

export default connect(mapStateToProps, actions)(AdminPage);
