import React from 'react';
import useAsyncEffect from 'use-async-effect';
import { connect } from 'react-redux';
import queryString from 'query-string';

import { getMenu } from '../../State/Actions/menuActions';
import ImgMediaCard from '../../Components/DishCard';
import MenuHeader from './Components/MenuHeader';
import Grafic1 from "./Grafics/grafic_1";
import Grafic2 from "./Grafics/grafic_2";
import Grafic3 from "./Grafics/grafic_3";
import Grafic4 from "./Grafics/grafic_4";

function MenuPage(props: any) {
  const { category } = queryString.parse(props.history.location.search);

  useAsyncEffect(async () => {
    await props.getMenu({
      category,
    });
  }, [category]);

  return (
      <div className="container-fluid mt-5">
          <MenuHeader
              findDishes={props.getMenu}
              category={category}
          />

          {category === 'Новости' && (
              <div className="d-flex">
                  <div className="d-flex flex-wrap justify-content-between">
                      {props.menuReducer.dishes.map((item, key) => (
                          <div className="mb-2 d-flex" key={key}>
                              <ImgMediaCard dish={item} />
                          </div>
                      ))}
                  </div>
              </div>
          )}

          {category === 'Статистика' && (
              <div>
                  <Grafic1/>
                  <Grafic4/>
                  <Grafic2/>
                  <Grafic3/>
              </div>
          )}
      </div>
  );
}

const mapStateToProps = state => ({
  menuReducer: state.menuReducer,
});

const actions = {
  getMenu,
};

export default connect(mapStateToProps, actions)(MenuPage);
