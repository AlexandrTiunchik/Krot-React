export const SORT = [
  { title: 'Рейтинг - от низкого к высокому', order: 'rating|1' },
  { title: 'Рейтинг - от высокого к низкому', order: 'rating|-1' },
];

export const CATEGORY_FILTER = [
  'Новости',
  'Статистика',
];
