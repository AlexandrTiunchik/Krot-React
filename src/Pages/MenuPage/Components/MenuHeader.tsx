import React, {useEffect, useState} from 'react';
import {
  Button, FormControl, MenuItem, Select,
} from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import { withRouter } from 'react-router-dom';

import styles from '../MenuPage.module.css';
import { CATEGORY_FILTER, SORT } from '../Constants/MenuHeader';
import queryString from "query-string";

function MenuHeader(props: any) {
  const { category } = queryString.parse(props.history.location.search);

  const [state, setState] = useState({
    category: props.category || '',
    sort: '',
  });

  const handleChange = (event: any): void => {
    setState({
      ...state,
      [event.target.name]: event.target.value,
    });
  };

  useEffect(() => {
    console.log(props)
    props.history.push({
      search: `?category=${state.category}`
    })
  }, [state.category])

  const findDishes = async () => {
    await props.findDishes(state);
  };

  return (
    <div className={`${styles['menu-header']} rounded d-flex`}>
      {category === 'Новости' && (
        <FormControl className="m-4">
          <InputLabel style={{color: 'white'}} htmlFor="age-label-placeholder">
            Сортировка
          </InputLabel>
          <Select
            className={styles.selector}
            style={{color: 'white'}}
            fullWidth
            title="Сортировка"
            name="sort"
            value={state.sort}
            onChange={handleChange}
          >
            <MenuItem value="">
              <em>Нет</em>
            </MenuItem>
            {SORT.map((item, key) => <MenuItem key={key} value={item.order}>{item.title}</MenuItem>)}
          </Select>
        </FormControl>
      )}

      <FormControl className="m-4">
        <InputLabel style={{color: 'white'}} htmlFor="age-label-placeholder">
          Категория
        </InputLabel>
        <Select
          fullWidth
          style={{color: 'white'}}
          className={styles.selector}
          title="Категория"
          name="category"
          value={state.category}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>Нет</em>
          </MenuItem>
          {CATEGORY_FILTER.map((item, key) => <MenuItem key={key} value={item}>{item}</MenuItem>)}
        </Select>
      </FormControl>

      {category === 'Новости' && (
        <Button
          onClick={findDishes}
          className={styles.button}
        >
          Найти
        </Button>
      )}
    </div>
  );
}

export default withRouter(MenuHeader);
