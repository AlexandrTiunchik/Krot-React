import React from "react";
import {
    PieChart, Pie, Sector, Cell, Tooltip, Legend, Bar, XAxis, YAxis, BarChart, CartesianGrid
} from 'recharts';

const data = [
    {
        "name": "2017 год",
        "uv": 127,
        "pv": 100,
    },
    {
        "name": "2018 год",
        "uv": 80,
        "pv": 140,
    },
    {
        "name": "2019 год",
        "uv": 127,
        "pv": 127,
    },
    {
        "name": "2020 год",
        "uv": 127,
        "pv": 0,
    },
];

export default function Grafic4 () {
    return (
        <div className="d-flex justify-content-center flex-column border">
            <h4 className="text-center mt-4 mb-4">Строительство подводящих газопроводов</h4>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#82CA9D'}} />
                <span className="ml-2">Факт, км</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#8884D8'}} />
                <span className="ml-2">План, км</span>
            </div>
            <div className="d-flex justify-content-center">
                <BarChart width={730} height={250} data={data}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="pv" fill="#8884d8" />
                    <Bar dataKey="uv" fill="#82ca9d" />
                </BarChart>
            </div>
        </div>
    )
}