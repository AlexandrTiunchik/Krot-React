import React from "react";
import {
    PieChart, Pie, Sector, Cell,
} from 'recharts';

const data = [
    { name: 'Group B', value: 109 },
    { name: 'Group C', value: 12 },
    { name: 'Group D', value: 13 },
    { name: 'Group D', value: 144 },
    { name: 'Group D', value: 53 },
    { name: 'Group D', value: 68 },
    { name: 'Group A', value: 601 },
];

const COLORS = ['#715b8c', '#4a9ab0', '#347a8b', '#93a7cb', '#46709f', '#a34543', '#839f4e'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
                                   cx, cy, midAngle, innerRadius, outerRadius, percent, index,
                               }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
};

export default function Grafic3 () {
    return (
        <div className="d-flex justify-content-center flex-column border">
            <h4 className="text-center mt-4 mb-4">Структура электропотребления в Беларуси</h4>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#4a9ab0'}} />
                <span className="ml-2">Непромышленные потребители</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#347a8b'}} />
                <span className="ml-2">Городской транспорт</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#93a7cb'}} />
                <span className="ml-2">Железнодорожный транспорт</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#46709f'}} />
                <span className="ml-2">Городское население</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#a34543'}} />
                <span className="ml-2">Сельское население</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#839f4e'}} />
                <span className="ml-2">Производственные сельскохозяйственные потребители</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#715b8c'}} />
                <span className="ml-2">Промышленные потребители</span>
            </div>
            <div className="d-flex justify-content-center">
                <PieChart width={360} height={380}>
                    <Pie
                        data={data}
                        labelLine={false}
                        label={renderCustomizedLabel}
                        outerRadius={150}
                        fill="#8884d8"
                        dataKey="value"
                    >
                        {
                            data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                        }
                    </Pie>
                </PieChart>
            </div>
        </div>
    )
}