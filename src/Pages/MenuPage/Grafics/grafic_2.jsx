import React from "react";
import {
    PieChart, Pie, Sector, Cell,
} from 'recharts';

const data = [
    { name: 'Group A', value: 600 },
    { name: 'Group B', value: 170 },
    { name: 'Group C', value: 30 },
    { name: 'Group D', value: 100 },
    { name: 'Group D', value: 50 },
    { name: 'Group D', value: 50 },
];

const COLORS = ['#715b8c', '#4a9ab0', '#347a8b', '#93a7cb', '#46709f', '#a34543', '#839f4e'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
                                   cx, cy, midAngle, innerRadius, outerRadius, percent, index,
                               }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
};

export default function Grafic2 () {
    return (
        <div className="d-flex justify-content-center flex-column border">
            <h4 className="text-center mt-4 mb-4">Баланс возобновляемых источников энергии в Беларуси</h4>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#715b8c'}} />
                <span className="ml-2">Топливная древесина</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#4a9ab0'}} />
                <span className="ml-2">Топливная щепа</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#347a8b'}} />
                <span className="ml-2">Древесные отходы</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#93a7cb'}} />
                <span className="ml-2">Прочие отходы</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#46709f'}} />
                <span className="ml-2">Энергия ветра</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#a34543'}} />
                <span className="ml-2">Энергия воды</span>
            </div>
            <div className="d-flex justify-content-center">
                <PieChart width={360} height={380}>
                    <Pie
                        data={data}
                        labelLine={false}
                        label={renderCustomizedLabel}
                        outerRadius={150}
                        fill="#8884d8"
                        dataKey="value"
                    >
                        {
                            data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                        }
                    </Pie>
                </PieChart>
            </div>
        </div>
    )
}