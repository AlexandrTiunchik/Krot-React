import React from "react";
import {
    PieChart, Pie, Sector, Cell,
} from 'recharts';

const data = [
    { name: 'Group A', value: 610 },
    { name: 'Group B', value: 45 },
    { name: 'Group C', value: 136 },
    { name: 'Group D', value: 83 },
    { name: 'Group D', value: 52 },
    { name: 'Group D', value: 32 },
    { name: 'Group D', value: 21 },
];

const COLORS = ['#715b8c', '#4a9ab0', '#347a8b', '#93a7cb', '#46709f', '#a34543', '#839f4e'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
                                   cx, cy, midAngle, innerRadius, outerRadius, percent, index,
                               }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
};

export default function Grafic1 () {
    return (
        <div className="d-flex justify-content-center flex-column border">
            <h4 className="text-center mt-4 mb-4">Структура валового потребления ТЭР в Республике Беларусь</h4>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#715b8c'}} />
                <span className="ml-2">Природный газ</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#4a9ab0'}} />
                <span className="ml-2">Мазут</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#347a8b'}} />
                <span className="ml-2">Светлые нефтепродукты</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#93a7cb'}} />
                <span className="ml-2">Другие виды топлива</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#46709f'}} />
                <span className="ml-2">Возобновляемые источники энергии</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#a34543'}} />
                <span className="ml-2">ВЭР</span>
            </div>
            <div className="ml-3 d-flex align-items-center mb-1">
                <div style={{width: 20, height: 20, backgroundColor: '#839f4e'}} />
                <span className="ml-2">Импорт электроэнергии</span>
            </div>
            <div className="d-flex justify-content-center">
                <PieChart width={360} height={380}>
                    <Pie
                        data={data}
                        labelLine={false}
                        label={renderCustomizedLabel}
                        outerRadius={150}
                        fill="#8884d8"
                        dataKey="value"
                    >
                        {
                            data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                        }
                    </Pie>
                </PieChart>
            </div>
        </div>
    )
}