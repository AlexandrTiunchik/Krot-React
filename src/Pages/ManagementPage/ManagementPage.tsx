import React, { useState } from 'react';
import { connect } from 'react-redux';
import useAsyncEffect from 'use-async-effect';

import ManagementTable from './Components/ManagementTable';
import styles from './ManagementPage.module.css';
import {
  addOrder, deleteOrder, getAllOrders, updateOrder,
} from '../../State/Actions/orderActions';
import ResponsiveDialog from '../../Components/ResponsiveDialog';
import OrderInfo from './Components/OrderInfo';
import UpdateOrderForm from './Components/UpdateOrderForm';
import orderReducer from "../../State/Reducers/orderReducer";

function ManagementPage(props: any) {
  const [state, setState] = useState({
    dialogType: '',
    isOpenDialog: false,
    order: null,
  });

  useAsyncEffect(async () => {
    await props.getAllOrders();
  }, []);

  const dialogSettings = {
    info: (
      <OrderInfo order={state.order} />
    ),
    update: (
      <UpdateOrderForm updateOrder={props.updateOrder} orderData={state.order} />
    ),
  };

  const onOpenDialog = (flag: boolean, dialogType: string, data?: any) => {
    setState({
      ...state,
      isOpenDialog: flag,
      dialogType,
      order: data,
    });
  };

  return (
    <div className={`container ${styles['management-container']}`}>
      <ResponsiveDialog
        title={state.dialogType}
        onOpenDialog={onOpenDialog}
        isOpen={state.isOpenDialog}
        data={props.orderReducer.orders}
      >
        {dialogSettings[state.dialogType]}
      </ResponsiveDialog>

      <ManagementTable
        onOpenDialog={onOpenDialog}
        deleteOrder={props.deleteOrder}
        data={props.orderReducer.orders}
      />
    </div>
  );
}

const mapStateToProps = state => ({
  orderReducer: state.orderReducer,
});

const actions = {
  getAllOrders,
  addOrder,
  deleteOrder,
  updateOrder,
};

export default connect(mapStateToProps, actions)(ManagementPage);
