import React, { useState } from 'react';
import { Button, MenuItem, Select } from '@material-ui/core';

import { ORDER_STATUS } from '../Configs/OrderStatus';
import styles from './ManagementTable.module.css';

function UpdateOrderForm(props) {
  const [state, setState] = useState({
    status: '',
    ...props.orderData,
  });

  const handleChange = (event: any): void => {
    setState({
      ...state,
      [event.target.name]: event.target.value,
    });
  };

  const submit = async e => {
    e.preventDefault();
    await props.updateOrder(state._id, state);
  };

  return (
    <div>
      <form className={styles['form-container']} onSubmit={submit}>
        <Select
          fullWidth
          name="status"
          id="status"
          title="Статус"
          value={state.status}
          onChange={event => handleChange(event)}
        >
          <MenuItem value="">
            <em>Нету</em>
          </MenuItem>
          {ORDER_STATUS.map((status, key) => <MenuItem key={key} value={status}>{status}</MenuItem>)}
        </Select>

        <Button type="submit">Сохранить</Button>
      </form>
    </div>
  );
}

export default UpdateOrderForm;
