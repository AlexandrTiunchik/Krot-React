import React from 'react';
import InfoIcon from '@material-ui/icons/Info';
import {
  Avatar, Divider,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  TextField,
} from '@material-ui/core';

import { ORDER_USER_INFO, ORDER_INFO } from '../Configs/OrderInfo';
import AdapterLink from '../../../Components/AdapterLink';
import { ROUTE_PATH } from '../../../Constants/router';

export default function OrderInfo(props: any) {
  return (
    <div>
      <p>Client</p>
      <Divider variant="fullWidth" />

      {ORDER_USER_INFO.map((profileKey, key) => (
        <TextField
          key={key}
          fullWidth
          disabled
          variant="filled"
          label={profileKey}
          margin="normal"
          value={props.order.user.profile[profileKey]}
        />
      ))}

      <p>Order</p>
      <Divider variant="fullWidth" />

      {ORDER_INFO.map((orderKey, key) => (
        <TextField
          key={key}
          fullWidth
          disabled
          variant="filled"
          label={orderKey}
          margin="normal"
          value={props.order[orderKey]}
        />
      ))}

      <p>Новость</p>
      <Divider variant="fullWidth" />

      <div>
        <List>
          {props.order.dishes.map((dish, key) => (
            <ListItem key={key}>
              <ListItemAvatar>
                <Avatar src={dish.dish.image} />
              </ListItemAvatar>
              <ListItemText
                primary={dish.dish.title}
                secondary=""
              />
              <ListItemSecondaryAction>
                <IconButton
                  target="_blank"
                  component={AdapterLink}
                  to={`${ROUTE_PATH.DISH}/${dish.dish._id}`}
                  edge="end"
                  aria-label="delete"
                >
                  <InfoIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </div>

      <TextField
        disabled
        margin="normal"
        value={`Total: ${props.order.price}`}
      />
    </div>
  );
}
