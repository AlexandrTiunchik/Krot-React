import React from 'react';
import MaterialTable from 'material-table';

import styles from './ManagementTable.module.css';

const tableColumns: any = [
  {
    field: 'name',
    title: 'Имя',
    render: (rowData: any) => <div className={styles.row}>{rowData.user.profile.name}</div>,
  },
  {
    field: 'surname',
    title: 'Фамилия',
    render: rowData => <div className={styles.row}>{rowData.user.profile.surname}</div>,
  },
  {
    title: 'Адрес',
    render: rowData => (
      <div className={styles.row}>
        {rowData.user.profile.city}
,
        {' '}
        {rowData.user.profile.address}
      </div>
    ),
  },
  {
    field: 'status',
    title: 'Статус',
    render: rowData => (
      <div
        className={`${styles.row} ${styles[rowData.status]} rounded`}
      >
        {rowData.status}
      </div>
    ),
  },
  {
    field: 'date',
    title: 'Дата',
    render: rowData => <div className={styles.row}>{rowData.date}</div>,
  },
  {
    title: 'Телефон',
    render: rowData => <div className={styles.row}>{rowData.user.profile.telephone || ''}</div>,
  },
  {
    title: 'Цена',
    render: rowData => <div className={styles.row}>{rowData.price}$</div>,
  },
];

function ManagementTable(props: any) {
  const tableActions: any = [
    {
      icon: 'edit',
      tooltip: 'Edit dish',
      onClick: (event, rowData) => {
        props.onOpenDialog(true, 'update', rowData);
      },
    },
    {
      icon: 'announcement',
      tooltip: 'View',
      onClick: (event, rowData) => {
        props.onOpenDialog(true, 'info', rowData);
      },
    },
  ];

  const tableToolBar: any = {
    Toolbar: () => (
      <div className="p-2">
        <div className={`${styles['table-name']} rounded`}>
          Заказ
        </div>
      </div>
    ),
  };

  const tableEditable: any = {
    isDeletable: () => true,
    onRowDelete: async (data: any) => {
      await props.deleteOrder(data._id);
    },
  };

  return (
    <div style={{ maxWidth: '100%' }}>
      <MaterialTable
        title="Заказ"
        columns={tableColumns}
        actions={tableActions}
        components={tableToolBar}
        editable={tableEditable}
        data={props.data}
      />
    </div>
  );
}

export default ManagementTable;
