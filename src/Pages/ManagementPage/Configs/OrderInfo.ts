export const ORDER_USER_INFO = [
    'name',
    'surname',
    'patronymic',
    'telephone',
    'city',
    'address',
];

export const ORDER_INFO = [
    'date',
    'status',
    'price'
];
