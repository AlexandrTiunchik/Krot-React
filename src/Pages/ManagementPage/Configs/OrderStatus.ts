export const ORDER_STATUS = [
  'pending',
  'processing',
  'complete',
  'canceled',
];
