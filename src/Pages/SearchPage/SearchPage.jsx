import React, { useEffect, useState } from 'react';
import queryString from 'query-string';
import useAsyncEffect from 'use-async-effect';
import { Link } from 'react-router-dom';
import SearchApi from '../../Services/Api/searchApi';
import { ROUTE_PATH } from '../../Constants/router';

export default function SearchPage(props) {
  const { search } = queryString.parse(props.history.location.search);

  const [state, setState] = useState([]);

  useAsyncEffect(async () => {
      const searchResponse = await new SearchApi().search(search);
      setState(searchResponse.data);
  }, []);

  return (
    <div className="container mt-5">
      {state && state.map((result) => (
        <Link
          className="d-flex align-items-center m-2"
          to={`${ROUTE_PATH.DISH}/${result._id}`}
          key={result._id}
        >
          <img className="mr-3" style={{ height: 100 }} src={result.image} />
          <h3>{result.title}</h3>
        </Link>
      ))}
    </div>
  );
}
