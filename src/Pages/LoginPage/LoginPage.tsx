import React from 'react';
import { Button } from '@material-ui/core';
import { TextValidator } from 'react-material-ui-form-validator';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import ILoginState from './ILoginState';
import LoginRequestModel from './LoginRequestModel';
import Form from '../../Components/Form';
import { loginUserAction } from '../../State/Actions/userActions';
import styles from './LoginPage.module.css';
import ErrorMessage from '../../Components/ErrorMessage';

function LoginPage(props) {
  const [state, setState] = React.useState<ILoginState | any>({
    loginName: '',
    password: '',
    formErrors: [],
  });

  const onSubmit = async () => {
    setState({
      ...state,
      formErrors: [],
    });

    const loginRequest = new LoginRequestModel(state.loginName, state.password);

    try {
      await props.loginUserAction(loginRequest, props.history);
    } catch (err) {
      setState({
        ...state,
        formErrors: [err.data],
      });
    }
  };

  return (
    <div className="container">
      <div className={styles.form}>
        <Form
          onSubmit={onSubmit}
          state={state}
          setState={setState}
        >
          {state.formErrors.map((err, key) => <ErrorMessage key={key}>{err.msg}</ErrorMessage>)}

          <TextValidator
            label="Имя"
            name="loginName"
            value={state.loginName}
            validators={[
              'required',
              'minStringLength:3',
              'maxStringLength:12',
            ]}
            errorMessages={[
              'Это поле обязательно к заполнению',
              'Необходимо больше 3 символов',
              'Необходимо меньше 12 символов',
            ]}
          />

          <TextValidator
            label="Пароль"
            name="password"
            type="password"
            value={state.password}
            validators={['required', 'minStringLength:6', 'maxStringLength:12']}
            errorMessages={[
              'Это поле обязательно к заполнению',
              'Необходимо больше 6 символов',
              'Необходимо меньше 12 символов',
            ]}
          />

          <Button type="submit">Вход</Button>
        </Form>
      </div>
    </div>
  );
}

const actions = {
  loginUserAction,
};

export default withRouter(connect(null, actions)(LoginPage));
