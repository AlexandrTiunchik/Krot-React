export default class LoginRequestModel {
    loginName: string;
    password: string;

    constructor(loginName: string, password: string) {
        this.loginName = loginName;
        this.password = password;
    }
}
