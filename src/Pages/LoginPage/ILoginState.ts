import IErrorMessage from "../../Interfaces/IErrorMessage";

export default interface ILoginState {
    loginName: string;
    password: string;
    formErrors?: IErrorMessage[]
}
