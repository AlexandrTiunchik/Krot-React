import React from 'react';
import {
  AppBar, Box, makeStyles, Tab, Tabs, Typography,
} from '@material-ui/core';
import Energy1 from "./Pages/Energy1";
import Energy2 from "./Pages/Energy2";

function TabPanel(props) {
  const {
    children, value, index, ...other
  } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function ResourcePage(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="container mt-5">
      <div className={classes.root}>
        <AppBar position="static">
          <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
            <Tab label="Возобновляемые ресурсы" {...a11yProps(0)} />
            <Tab label="Невозобновляемые ресурсы" {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Energy2/>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Energy1/>
        </TabPanel>
      </div>
    </div>
  );
}
