import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import { Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';

import Form from '../../../Components/Form';

export default function Review(props) {
  const [state, setState] = useState({
    text: '',
    rating: '',
  });

  const submit = async () => {
    await props.createComment(state, props.dishId);
  };

  return (
    <Form
      onSubmit={submit}
      state={state}
      setState={setState}
    >

      <Box
        component="fieldset"
        mb={0}
        borderColor="black"
        className="ml-2"
      >
        <Typography
          className="ml-1"
          component="legend"
        >
          Оценка новсти
        </Typography>
        <Rating
          value={state.rating}
          name="rating"
        />
      </Box>

      <TextField
        name="text"
        label="Ваш коментарий"
        multiline
        rowsMax="4"
        value={state.comment}
        margin="normal"
      />

      <Button type="submit">Отправить</Button>
    </Form>
  );
}
