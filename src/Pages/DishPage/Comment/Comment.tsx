import React from 'react';
import Rating from '@material-ui/lab/Rating';

export default function Comment(props: any) {
  return (
    <div>
      {props.comments.map((comment, key) => (
          <div
            className="border rounded p-2 mb-2"
            key={key}
          >
            <span className="ml-1">
              <b>
                {comment.user.profile.name} {comment.user.profile.surname}
              </b>
            </span>
            <br />
            <Rating
              disabled
              value={comment.rating}
              name="rating"
            />
            <p className="ml-1">{comment.text}</p>
            {props.children}
          </div>
      ))}
    </div>
  );
}
