import React, { useState } from 'react';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import {
  Breadcrumbs, Button, Chip, Divider, TextField,
} from '@material-ui/core';
import { connect } from 'react-redux';
import Link from '@material-ui/core/Link';
import useAsyncEffect from 'use-async-effect';

import { getDish } from '../../State/Actions/dishActions';
import styles from './DishPage.module.css';
import AdapterLink from '../../Components/AdapterLink';
import ProtectedComponent from '../../Components/ProtectedComponent';
import { USER_ROLE } from '../../Constants/user';
import { updateBasket } from '../../State/Actions/basketActions';
import ErrorMessage from '../../Components/ErrorMessage';
import Review from './Comment/Review';
import Comment from './Comment/Comment';
import {
  createComment, deleteComment, getComments, getUserComment,
} from '../../State/Actions/commentActions';
import { ROUTE_PATH } from '../../Constants/router';

function DishPage(props: any) {
  const [state, setState] = useState<any | any>({
    dish: {
      image: '',
      title: '',
      description: '',
      price: 0,
      ingredients: [],
      category: '',
    },
    error: [],
    count: 1,
    price: 0,
    comments: [],
  });

  useAsyncEffect(async () => {
    props.getDish(props.match.params.id)
      .then(dish => setState({
        ...state,
        dish: dish.data,
        price: dish.data.price,
      }));
    await props.getComments(props.match.params.id);
    await props.getUserComment({
      dishId: props.match.params.id,
    });
  }, []);

  const deleteUserComment = async () => {
    await props.deleteComment(props.match.params.id);
  };

  const changeCount = event => {
    setState({
      ...state,
      [event.target.name]: event.target.value,
      price: event.target.value * state.dish.price,
    });
  };

  const submit = async () => {
    try {
      await props.updateBasket(state);
    } catch (err) {
      setState({
        ...state,
        error: [err.response.data],
      });
    }
  };

  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col-12 mb-5 d-flex justify-content-center">
          <Breadcrumbs aria-label="breadcrumb">
            <Link component={AdapterLink} color="inherit" to={ROUTE_PATH.MAIN_PAGE}>
              Главная
            </Link>
            <Link
              component={AdapterLink}
              color="inherit"
              to={`${ROUTE_PATH.MENU}?category=${state.dish.category}`}>
              {state.dish.category}
            </Link>
            <Link
              color="textPrimary"
              aria-current="page"
            >
              {state.dish.title}
            </Link>
          </Breadcrumbs>
        </div>

        <div className="col-12 col-md-6 text-center">
          <img
            alt={state.dish.title}
            className="img-thumbnail"
            src={state.dish.image}
          />
        </div>

        <div className="col-12 col-md-6">
          <h1>{state.dish.title}</h1>
          <p>{state.dish.description}</p>

          <div className="row mb-5">
            <ProtectedComponent
              currentRole={props.userReducer.role}
              role={[USER_ROLE.MANAGER, USER_ROLE.USER, USER_ROLE.ADMIN]}
            >
              <>
                <div className="col-12 d-flex justify-content-center">
                  {state.error.map((err, key) => <ErrorMessage key={key}>{err.msg}</ErrorMessage>)}
                </div>
              </>
            </ProtectedComponent>

          </div>
        </div>

      </div>

      <ProtectedComponent
        currentRole={props.userReducer.role}
        role={[USER_ROLE.MANAGER, USER_ROLE.USER, USER_ROLE.ADMIN]}
      >
        <div className="row">
          <div className="col-12">
            {props.commentReducer.userComment.length > 0
              ? (
                <div>
                  <h3>Ваш коментарий</h3>
                  <Comment
                    comments={props.commentReducer.userComment}
                  >
                    <div className="text-right">
                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={deleteUserComment}
                      >
                        Удалить
                      </Button>
                    </div>
                  </Comment>
                </div>
              )
              : (
                <Review
                  createComment={props.createComment}
                  dishId={props.match.params.id}
                />
              )}
          </div>
        </div>
      </ProtectedComponent>

      <div className="row">
        <div className="col-12 mt-5">
          <Comment
            comments={props.commentReducer.comments}
          />
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
  commentReducer: state.commentReducer,
});

const actions = {
  getDish,
  updateBasket,
  getComments,
  deleteComment,
  getUserComment,
  createComment,
};

export default connect(mapStateToProps, actions)(DishPage);
