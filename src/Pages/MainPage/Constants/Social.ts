import {
  faVk, faFacebook, faInstagram, faTwitter,
} from '@fortawesome/free-brands-svg-icons';

export const SOCIAL = [
  {
    name: 'vk',
    link: '/',
    icon: faVk,
  },
  {
    name: 'fb',
    link: '/',
    icon: faFacebook,
  },
  {
    name: 'tw',
    link: '/',
    icon: faTwitter,
  },
  {
    name: 'inst',
    link: '/',
    icon: faInstagram
  },
];
