import { ROUTE_PATH } from '../../../Constants/router';

export const MENU = [
  {
    img: 'https://image.flaticon.com/icons/png/512/21/21782.png',
    title: 'Новости',
    link: `${ROUTE_PATH.MENU}?category=Новости`,
  },
  {
    img: 'https://image.flaticon.com/icons/png/512/126/126425.png',
    title: 'Статистка',
    link: `${ROUTE_PATH.MENU}?category=Статистика`,
  },
  {
    img: 'https://image.flaticon.com/icons/png/512/58/58960.png',
    title: 'Энерго-Карта',
    link: `${ROUTE_PATH.MAP}`,
  },
  {
    img: 'https://www.flaticon.com/premium-icon/icons/svg/2191/2191317.svg',
    title: 'Ресурсы',
    link: `${ROUTE_PATH.RESOURCE}`,
  },
];
