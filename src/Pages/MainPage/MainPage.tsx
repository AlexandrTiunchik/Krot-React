import React from 'react';
import { connect } from 'react-redux';
import useAsyncEffect from 'use-async-effect';
import { getPopularDishes } from '../../State/Actions/dishActions';
import { getNews } from '../../State/Actions/newsActions';

import Stepper from '../../Components/Stepper';
import styles from './MainPage.module.css';
import ImgMediaCard from '../../Components/DishCard';
import Footer from './Components/Footer/Footer';

function MainPage(props: any) {
  useAsyncEffect(async () => {
    await props.getPopularDishes();
    await props.getNews({ count: 4 });
  }, []);

  return (
    <div className="container-fluid">
      <h1 className="text-center mt-4">МИР ЭНЕРГОСБЕРЕЖЕНИЯ</h1>
      <div className={styles.stepper}>
        <video className={styles.video} src="videoplayback.mp4" autoPlay />
        <Stepper data={props.newsReducer.news} />
        <h3 className={styles.popular}>Популярные новости</h3>
        <div className="d-flex flex-wrap justify-content-around">
          {props.dishReducer.popularDishes.map((item, key) => (
            <ImgMediaCard text dish={item.dish} key={key} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = (state) => ({
  dishReducer: state.dishReducer,
  newsReducer: state.newsReducer,
});

const actions = {
  getPopularDishes,
  getNews,
};

export default connect(mapStateToProps, actions)(MainPage);
