import React from 'react';
import { Link } from 'react-router-dom';

import { MENU } from '../../Constants/Menu';
import styles from './Menu.module.css';

export default function Menu() {
  return (
    <div className="d-flex">
      {MENU.map((item, key) => (
        <Link
          className={`d-flex flex-grow-1 align-items-center flex-column ${styles['menu-item']}`}
          to={item.link}
          key={key}
        >
          <img alt={item.title} className={styles.icon} src={item.img} />
          <span>{item.title}</span>
        </Link>
      ))}
    </div>
  );
}
