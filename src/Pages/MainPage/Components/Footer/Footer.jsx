import React from 'react';
import styles from './Footer.module.css';

export default function Footer() {
  const currentDate = () => {
    const date = new Date();

    return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}г.`;
  }

  return (
    <footer className={`${styles.footer} p-2 mt-2 d-flex flex-column align-items-center mb-5`}>
      <h4>{currentDate()}</h4>
    </footer>
  );
}
