import React from 'react';
import PersonPinIcon from '@material-ui/icons/PersonPin';

import TabPanel from '../../Components/TabPanel';
import Profile from './Components/Profile/Profile';
import styles from './ProfilePage.module.css';
import History from './Components/History/History';

const profile = {
  tab: [
    { label: 'Профиль', icon: <PersonPinIcon /> },
  ],
  tabContent: [
    { component: <Profile />, index: 0 },
    { component: <History />, index: 1 },
  ],
};

export default function ProfilePage() {
  return (
    <div className={`container ${styles['user-profile']}`}>
      <TabPanel profile={profile} />
    </div>
  );
}
