import React from 'react';
import { connect } from 'react-redux';
import useAsyncEffect from 'use-async-effect';

import ProfileTable from './HistoryTable';
import { getUserOrders } from '../../../../State/Actions/orderActions';

function History(props: any) {
  useAsyncEffect(async () => {
    await props.getUserOrders(props.userReducer.id);
  }, []);

  return (
    <div>
      <ProfileTable orders={props.userReducer.orders} />
    </div>
  );
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
});

const actions = {
  getUserOrders,
};

export default connect(mapStateToProps, actions)(History);
