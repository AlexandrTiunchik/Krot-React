import React from "react";
import MaterialTable from "material-table";
import {
    Avatar,
    IconButton,
    List,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText
} from "@material-ui/core";
import InfoIcon from "@material-ui/icons/Info";

import AdapterLink from "../../../../Components/AdapterLink";
import {ROUTE_PATH} from "../../../../Constants/router";

export default function HistoryTable(props) {
    return (
        <div>
            <MaterialTable
                title="Order history"
                columns={[
                    { title: 'Date', field: 'date' },
                    { title: 'Status', field: 'status' },
                    { title: 'Price', field: 'price' },
                ]}
                data={props.orders}

                detailPanel={((rowData: any) => {
                    return (
                        <List>
                            {rowData.dishes.map((dish, key) => {
                                return (
                                    <ListItem key={key}>
                                        <ListItemAvatar>
                                            <Avatar src={dish.dish.image}/>
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={dish.dish.title}
                                            secondary=''>
                                        </ListItemText>
                                        <ListItemText
                                            primary={'Price: ' + dish.dish.price}
                                            secondary={'Count: ' + dish.count}>
                                        </ListItemText>
                                        <ListItemSecondaryAction>
                                            <IconButton
                                                target="_blank"
                                                component={AdapterLink}
                                                to={`${ROUTE_PATH.DISH}/${dish.dish._id}`}
                                                edge="end"
                                                aria-label="delete"
                                            >
                                                <InfoIcon/>
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                )
                            })}
                        </List>
                    )
                })}
            />
        </div>
    )
}
