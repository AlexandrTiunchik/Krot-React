import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Button, TextField } from '@material-ui/core';

import Form from '../../../../Components/Form';
import { getUserProfileInfo, updateProfile } from '../../../../State/Actions/userActions';
import styles from './Profile.module.css';

function Profile(props: any) {
  const [state, setState] = useState({
    name: '',
    surname: '',
    patronymic: '',
    telephone: '',
    city: '',
    address: '',
  });

  useEffect(() => {
    props.getUserProfileInfo().then(profile => setState({
      ...state,
      ...profile,
    }));
  }, []);

  const onSubmit = async () => {
    await props.updateProfile(state);
  };

  return (
    <div className={styles.profile}>
      <Form
        onSubmit={onSubmit}
        state={state}
        setState={setState}
      >
        <TextField
          label="Имя"
          value={state.name}
          name="name"
          margin="normal"
          variant="outlined"
        />

        <TextField
          label="Фамилия"
          value={state.surname}
          name="surname"
          margin="normal"
          variant="outlined"
        />

        <TextField
          label="Телефон"
          name="telephone"
          value={state.telephone}
          margin="normal"
          variant="outlined"
        />

        <TextField
          label="Город"
          name="city"
          value={state.city}
          margin="normal"
          variant="outlined"
        />

        <TextField
          label="Адрес"
          name="address"
          value={state.address}
          margin="normal"
          variant="outlined"
        />

        <Button type="submit">Сохранить</Button>
      </Form>
    </div>
  );
}

const mapStateToProps = state => ({
  user: state.user,
});

const actions = {
  getUserProfileInfo,
  updateProfile,
};

export default connect(mapStateToProps, actions)(Profile);
