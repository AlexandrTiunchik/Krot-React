import React, {Component} from 'react';
import {Map, GoogleApiWrapper, Marker, InfoWindow} from 'google-maps-react';
import AdapterLink from '../../../Components/AdapterLink';
import Button from '@material-ui/core/Button';
import { ROUTE_PATH } from '../../../Constants/router';
import {BrowserRouter, Link} from "react-router-dom";

interface Marker {
    dish: {
        title: string,
        name: string,
        image: string,
        description: string,
        _id: string
    }
}

interface Props {
    google: object,
    initialCenter: object,
    zoom: object,
    marker: any,
    resurses: any,
}


class GoogleMap extends Component<Props> {
    state = {
        showingInfoWindow: false,
        activeMarker: {},
        selectedPlace: {
            name: '',
            description: '',
            image: '',
            id: '',
            url: ''
        },
    };

    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            })
        }
    };

    onMarkerClick = (props, marker, e) => {
      console.log(props);
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        })};

    render() {
      //console.log(this.state)
        return (
            <div>
                <Map
                    className='map'
                    google={this.props.google}
                    initialCenter={this.props.initialCenter}
                    zoom={this.props.zoom}
                >

                    {this.props.marker.map((vul, key) => {
                        const marker = {
                            lat: vul.lat,
                            lng: vul.lng
                        };

                        return (<Marker
                            icon={{
                                url: 'factory.png',
                            }}
                            key={key}
                            position={marker}
                            name={vul.title}
                            url={vul.url}
                            onClick={this.onMarkerClick}
                        />)
                    })}

                    {this.props.resurses.map((vul, key) => {
                        const marker = {
                            lat: vul.lat,
                            lng: vul.lng
                        };

                        return (<Marker
                            icon={{
                                url: 'hammer.png',
                            }}
                            key={key}
                            position={marker}
                            name={vul.title}
                            url={vul.id}
                            onClick={this.onMarkerClick}
                        />)
                    })}

                    <InfoWindow
                        marker={this.state.activeMarker}
                        visible={this.state.showingInfoWindow}>
                        <div>
                          {typeof this.state.selectedPlace.url === 'string' ?
                            <a target='_blank' href={this.state.selectedPlace.url}>
                              <h1>{this.state.selectedPlace.name}</h1></a>
                            : <h1>{this.state.selectedPlace.name}</h1>
                          }
                        </div>
                    </InfoWindow>
                </Map>
            </div>
        );
    };
};

const style = {
    width: '70%',
    height: '100%'
};

const LoadingContainer = (props) => (
    <div>Fancy loading container</div>
);

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAT7eiqhZp2fC-8dcB3qEkHsL97IUXk0R4',
    LoadingContainer: LoadingContainer
})(GoogleMap);
