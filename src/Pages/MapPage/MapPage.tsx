import React from 'react';
import { getPopularDishes } from '../../State/Actions/dishActions';
import { connect } from 'react-redux';
import GoogleMap from "./Components/GoogleMap";
import styles from './styles.module.css';
import {FACTORY} from "../../Constants/factory";
import {RESURSES} from "../../Constants/resurses";

function Map(props) {

    const initialFocus = {
        lat: 53.665451,
        lng: 23.821653,
    };

    return (
        <div style={{backgroundColor: 'black'}}>
            <div
              className={styles.map}
            >
                <GoogleMap
                    initialCenter={initialFocus}
                    zoom={10}
                    marker={FACTORY}
                    resurses={RESURSES}
                />
            </div>
            <div className={styles.footer}/>
        </div>
    )
}

const mapStateToProps = state => ({
    dishReducer: state.dishReducer,
  });
  
const actions = {
    getPopularDishes,
};

export default connect(mapStateToProps, actions)(Map);
