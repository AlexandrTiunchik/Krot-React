import React from 'react';
import { connect } from 'react-redux';
import {
  Avatar,
  Button,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText, Paper, Typography,
} from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import useAsyncEffect from 'use-async-effect';
import { faShoppingBasket } from '@fortawesome/free-solid-svg-icons';

import { deleteItem, getBasket } from '../../State/Actions/basketActions';
import AdapterLink from '../../Components/AdapterLink';
import { ROUTE_PATH } from '../../Constants/router';
import { addOrder } from '../../State/Actions/orderActions';

function BasketPage(props: any) {
  useAsyncEffect(async () => {
    await props.getBasket();
  }, []);

  const submit = async () => {
    await props.addOrder();
    await props.getBasket();
  };

  const deleteDish = (dish: any) => async () => {
    const updateDishes = props.basketReducer.basket.dishes.filter(datum => datum.id !== dish.id);
    await props.deleteItem(updateDishes);
  };

  return (
    <div className="container mt-5">
      {props.basketReducer.basket && props.basketReducer.basket.dishes.length > 0
        ? (
          <div className="row">
            <div className="col">
              <List>
                {props.basketReducer.basket.dishes.map((item, key) => (
                  <div key={key}>
                    <ListItem className="row d-flex">
                      <ListItemAvatar>
                        <Avatar src={item.dish.image} />
                      </ListItemAvatar>
                      <ListItemText
                        primary={item.dish.title}
                      />
                      <ListItemText
                        primary={`Price: ${item.dish.price}$`}
                        secondary={`Count: ${item.count}`}
                      />
                      <ListItemText
                        primary={`Total: ${item.dish.price * item.count}$`}
                      />
                    </ListItem>
                    <div className="d-flex justify-content-end mr-2 mb-2">
                      <Button
                        className="mr-2"
                        variant="contained"
                        target="_blank"
                        component={AdapterLink}
                        to={`${ROUTE_PATH.DISH}/${item.dish._id}`}
                      >
                        Information
                      </Button>
                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={deleteDish(item)}
                      >
                        Delete
                      </Button>
                    </div>
                    <Divider variant="middle" />
                  </div>
                ))}
              </List>
            </div>

            <div className="rounded p-3 border col-md-3
                d-flex flex-column justify-content-center text-center"
            >
              <h3>Your Basket</h3>
              <p>
                Items: {props.basketReducer.basket.dishes.length}
              </p>
              <p>
                Price: {props.basketReducer.basket.price}$
              </p>
              <Button
                variant="contained"
                color="primary"
                onClick={submit}
              >
                  Buy
              </Button>
            </div>
          </div>
        )
        : (
          <div className="container d-flex justify-content-center align-items-center">
            <Paper elevation={0} className="text-center">
              <Typography variant="h2" gutterBottom>
                  Basket is empty
              </Typography>
              <FontAwesomeIcon size="4x" icon={faShoppingBasket} />
            </Paper>
          </div>
        )}
    </div>
  );
}

const mapStateToProps = state => ({
  basketReducer: state.basketReducer,
});

const actions = {
  getBasket,
  addOrder,
  deleteItem,
};


export default connect(mapStateToProps, actions)(BasketPage);
