import React, { useEffect } from 'react';
import { Button } from '@material-ui/core';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import styles from '../LoginPage/LoginPage.module.css';
import IRegistrationState from './IRegistrationState';
import RegistrationRequestModel from './RegistrationRequestModel';
import Form from '../../Components/Form';
import { registrationUser } from '../../State/Actions/userActions';
import ErrorMessage from '../../Components/ErrorMessage';

function RegistrationPage(props: any) {
  const [state, setState] = React.useState<IRegistrationState | any>({
    loginName: '',
    email: '',
    password: '',
    confirmPassword: '',
    formErrors: [],
  });

  useEffect(() => {
    ValidatorForm.addValidationRule('isPasswordMatch', (value: any): boolean => value === state.password);
  });

  const onSubmit = async () => {
    setState({
      ...state,
      formErrors: [],
    });

    const registrationRequest = new RegistrationRequestModel(
      state.loginName, state.email, state.password,
    );

    try {
      await props.registrationUser(registrationRequest, props.history);
    } catch (err) {
      setState({
        ...state,
        formErrors: [err.response.data],
      });
    }
  };

  return (
    <div className="container">
      <div className={styles.form}>
        <Form
          onSubmit={onSubmit}
          state={state}
          setState={setState}
        >
          {state.formErrors.map((err, key) => <ErrorMessage key={key}>{err.msg}</ErrorMessage>)}

          <TextValidator
            label="Имя"
            name="loginName"
            value={state.loginName}
            validators={['required', 'minStringLength:3', 'maxStringLength:12']}
            errorMessages={[
              'Это поле обязательно к заполнению',
              'Необходимо больше 3 символов',
              'Необходимо меньше 12 символов',
            ]}
          />

          <TextValidator
            label="Email"
            name="email"
            value={state.email}
            validators={['required', 'isEmail']}
            errorMessages={[
              'Это поле обязательно к заполнению',
              'Некоректный email',
            ]}
          />

          <TextValidator
            label="Пароль"
            name="password"
            type="password"
            value={state.password}
            validators={['required', 'minStringLength:6', 'maxStringLength:12']}
            errorMessages={[
              'Это поле обязательно к заполнению',
              'Необходимо больше 6 символов',
              'Необходимо меньше 12 символов',
            ]}
          />

          <TextValidator
            label="Подтверждение пароля"
            name="confirmPassword"
            type="password"
            value={state.confirmPassword}
            validators={['required', 'minStringLength:6', 'maxStringLength:12', 'isPasswordMatch']}
            errorMessages={[
              'Это поле обязательно к заполнению',
              'Необходимо больше 6 символов',
              'Необходимо меньше 12 символов',
              'Пароли не совпадают',
            ]}
          />

          <Button type="submit">Регистрация</Button>
        </Form>
      </div>
    </div>
  );
}

const actions = {
  registrationUser,
};

export default withRouter(connect(null, actions)(RegistrationPage));
