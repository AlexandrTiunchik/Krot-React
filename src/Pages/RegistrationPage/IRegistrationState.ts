import IErrorMessage from "../../Interfaces/IErrorMessage";

export default interface IRegistrationState {
    loginName: string;
    email: string;
    password: string;
    confirmPassword: string;
    formErrors: IErrorMessage[];
}
