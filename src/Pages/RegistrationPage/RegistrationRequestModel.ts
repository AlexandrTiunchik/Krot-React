export default class LoginRequestModel {
    loginName: string;
    email: string;
    password: string;

    constructor(login: string, email: string, password: string) {
        this.loginName = login;
        this.email = email;
        this.password = password;
    }
}
