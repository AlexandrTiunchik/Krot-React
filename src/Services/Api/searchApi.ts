import Api from './api';

export default class SearchApi extends Api {
  readonly SEARCH_URL = '/api/search';

  async search(search) {
    return this.get(`${this.SEARCH_URL}?search=${search}`);
  }
}
