import Api from './api';
import { IOrder } from '../../Interfaces/IOrder';

export default class OrderApi extends Api {
  readonly ORDER_URL = '/api/order/';

  async deleteOrder(id: string): Promise<any> {
    return this.delete(this.ORDER_URL + id);
  }

  async updateOrder(id: string, data: any): Promise<any> {
    return this.put(this.ORDER_URL + id, data);
  }

  async getSingleOrder(id: string): Promise<any> {
    return this.get(this.ORDER_URL + id);
  }

  async getAllOrders(): Promise<any> {
    return this.get(`${this.ORDER_URL}all`);
  }

  async addOrder(data?: IOrder): Promise<any> {
    return this.post(this.ORDER_URL, data);
  }

  async getUserOrder(id: string): Promise<any> {
    return this.get(`${this.ORDER_URL}user/${id}`);
  }
}
