import axios from 'axios';
import { API_BASE_URL } from '../../Configs/apiConfigs';

export default class Api {
  async post(url: string, data: any): Promise<any> {
    return axios.post(API_BASE_URL + url, data);
  }

  async get(url: string, params?: any): Promise<any> {
    return axios.get(API_BASE_URL + url, params);
  }

  async put(url: string, data: any): Promise<any> {
    return axios.put(API_BASE_URL + url, data);
  }

  async delete(url: string): Promise<any> {
    return axios.delete(API_BASE_URL + url);
  }
}
