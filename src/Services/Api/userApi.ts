import Api from './api';
import LoginRequestModel from '../../Pages/LoginPage/LoginRequestModel';
import RegistrationRequestModel from '../../Pages/RegistrationPage/RegistrationRequestModel';
import { IUserProfile } from '../../State/Reducers/userReducer';

export default class UserApi extends Api {
  readonly AUTH_URL = '/api/authentication/';

  readonly PROFILE_URL = '/api/profile/';

  readonly USER_URL ='/api/user/';

  async login(data: LoginRequestModel): Promise<any> {
    return this.post(`${this.AUTH_URL}login`, data);
  }

  async registration(data: RegistrationRequestModel): Promise<any> {
    return this.post(`${this.AUTH_URL}registration`, data);
  }

  async getProfileInfo(): Promise<any> {
    return this.get(this.PROFILE_URL);
  }

  async updateProfile(profile: IUserProfile): Promise<any> {
    return this.put(this.PROFILE_URL, profile);
  }

  async getUser(): Promise<any> {
    return this.get(this.USER_URL);
  }
}
