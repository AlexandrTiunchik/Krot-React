import Api from './api';

export default class BasketApi extends Api {
  readonly DISH_URL = '/api/basket/';

  async updateBasket(data: any): Promise<any> {
    return this.put(this.DISH_URL, data);
  }

  async deleteItem(data: any): Promise<any> {
    return this.put(`${this.DISH_URL}delete`, data);
  }

  async getBasket(): Promise<any> {
    return this.get(this.DISH_URL);
  }
}
