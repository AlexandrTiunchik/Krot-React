import Api from './api';

export default class MenuApi extends Api {
  readonly MENU_URL = '/api/menu/';

  async getMenu(params?: any): Promise<any> {
    return this.get(this.MENU_URL, { params });
  }
}
