import Api from './api';
import IDish from '../../Interfaces/IDish';

export default class NewsApi extends Api {
  readonly NEWS_URL = '/api/news/';

  async getNews(params?: any): Promise<any> {
    return this.get(`${this.NEWS_URL}`, { params });
  }

  async addNews(data: IDish): Promise<any> {
    return this.post(this.NEWS_URL, data);
  }

  async deleteNews(id: string): Promise<any> {
    return this.delete(`${this.NEWS_URL}${id}`);
  }

  async updateNews(id: string, data: any): Promise<any> {
    return this.put(`${this.NEWS_URL}${id}`, data);
  }
}
