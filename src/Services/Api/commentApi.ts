import Api from './api';

export default class CommentApi extends Api {
  readonly DISH_URL = '/api/comment/';

  async deleteComment(id: string): Promise<any> {
    return this.delete(`${this.DISH_URL}${id}`);
  }

  async getComments(id: string): Promise<any> {
    return this.get(`${this.DISH_URL}${id}`);
  }

  async getUserComment(params: any): Promise<any> {
    return this.get(`${this.DISH_URL}user`, { params });
  }

  async createComment(data: any, id: string): Promise<any> {
    return this.post(`${this.DISH_URL}${id}`, data);
  }
}
