import Api from './api';
import IDish from '../../Interfaces/IDish';

export default class DishApi extends Api {
  readonly DISH_URL = '/api/dishes/';

  async deleteDish(id: string): Promise<any> {
    return this.delete(this.DISH_URL + id);
  }

  async updateDish(id: string, data: IDish): Promise<any> {
    return this.put(this.DISH_URL + id, data);
  }

  async getSingleDish(id: string): Promise<any> {
    return this.get(this.DISH_URL + id);
  }

  async getAllDish(): Promise<any> {
    return this.get(`${this.DISH_URL}all`);
  }

  async addDish(data: IDish): Promise<any> {
    return this.post(this.DISH_URL, data);
  }

  async getDishes(): Promise<any> {
    return this.get(this.DISH_URL);
  }
}
