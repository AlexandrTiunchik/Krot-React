import React from 'react';
import Button from '@material-ui/core/Button';

import AdapterLink from '../Components/AdapterLink';
import { ROUTE_PATH } from '../Constants/router';
import { USER_ROLE } from '../Constants/user';
import ProtectedComponent from '../Components/ProtectedComponent';

const ROLE = {
  user: (
    <div>
      <Button color="inherit" component={AdapterLink} to={ROUTE_PATH.PROFILE}>Профиль</Button>
    </div>
  ),
  admin: (
    <div>
      <Button color="inherit" component={AdapterLink} to={ROUTE_PATH.PROFILE}>Профиль</Button>
      <Button color="inherit" component={AdapterLink} to={ROUTE_PATH.ADMIN}>Менеджер</Button>
    </div>
  ),
  guest: (
    <div>
      <Button color="inherit" component={AdapterLink} to={ROUTE_PATH.LOGIN}>Вход</Button>
      <Button color="inherit" component={AdapterLink} to={ROUTE_PATH.REGISTRATION}>Регистрация</Button>
    </div>
  ),
};

export default function HeaderHOC(Component) {
  return function HeaderContainer(props: any) {
    const { currentRole } = props;
    const logoutButton = (
      <ProtectedComponent
        currentRole={currentRole}
        role={[USER_ROLE.ADMIN, USER_ROLE.USER, USER_ROLE.MANAGER]}
      >
        <Button color="inherit" onClick={() => props.logout(props.history)}>Выйти</Button>
      </ProtectedComponent>
    );

    const rightPanel = ROLE[currentRole] || ROLE.guest;

    return <Component logout={logoutButton} rightPanel={rightPanel} />;
  };
}
