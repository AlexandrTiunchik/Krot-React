import {connect} from 'react-redux';
import React from "react";

export default function StoreHOC(Component, actions?: {}) {
    const mapStateToProps = (state) => ({
        user: state.user,
        dish: state.dish,
        order: state.order
    });

    return connect(mapStateToProps, actions)(Component)
}
