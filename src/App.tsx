import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import useAsyncEffect from 'use-async-effect';

import RegistrationPage from './Pages/RegistrationPage/RegistrationPage';
import MainPage from './Pages/MainPage/MainPage';
import LoginPage from './Pages/LoginPage/LoginPage';
import Header from './Components/Header';
import { ROUTE_PATH } from './Constants/router';
import { ProtectedRoute } from './Components/ProtectedRoute';
import ProfilePage from './Pages/ProfilePage/ProfilePage';
import AdminPage from './Pages/AdminPage/AdminPage';
import { getUser, logoutUser } from './State/Actions/userActions';
import { USER_ROLE } from './Constants/user';
import DishPage from './Pages/DishPage/DishPage';
import MenuPage from './Pages/MenuPage/MenuPage';
import MapPage from './Pages/MapPage/MapPage';
import SearchPage from './Pages/SearchPage/SearchPage';
import ResourcePage from './Pages/ResourcePage/Resource';
import './App.css';
import MapInfoPage from "./Pages/MapInfoPage/MapInfoPage";

function App(props: any) {
  useAsyncEffect(async () => {
    await props.getUser(props.history);
  }, []);

  const { role } = props.userReducer;

  return (
    <div className="App">
      <Header logout={props.logoutUser} currentRole={role} />
      <Route exact path={ROUTE_PATH.MAIN_PAGE} component={MainPage} />
      <Route exact path={`${ROUTE_PATH.MAP_INFO}/:id`} component={MapInfoPage} />
      <Route exact path={`${ROUTE_PATH.DISH}/:id`} component={DishPage} />
      <Route exact path={ROUTE_PATH.MENU} component={MenuPage} />
      <Route exact path={ROUTE_PATH.MAP} component={MapPage} />
      <Route exact path={ROUTE_PATH.RESOURCE} component={ResourcePage} />
      <Route exact path={ROUTE_PATH.SEARCH} component={(state) => <SearchPage {...state}/>} />
      <ProtectedRoute
        currentRole={role}
        exact
        role={[USER_ROLE.GUEST]}
        path={ROUTE_PATH.LOGIN}
        component={LoginPage}
      />
      <ProtectedRoute
        currentRole={role}
        exact
        role={[USER_ROLE.GUEST]}
        path={ROUTE_PATH.REGISTRATION}
        component={RegistrationPage}
      />
      <ProtectedRoute
        currentRole={role}
        exact
        role={[USER_ROLE.USER, USER_ROLE.MANAGER, USER_ROLE.ADMIN]}
        path={ROUTE_PATH.PROFILE}
        component={ProfilePage}
      />
      <ProtectedRoute
        currentRole={role}
        exact
        role={[USER_ROLE.ADMIN]}
        path={ROUTE_PATH.ADMIN}
        component={AdminPage}
      />
    </div>
  );
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
});

const actions = {
  logoutUser,
  getUser,
};

export default withRouter(connect(mapStateToProps, actions)(App));
