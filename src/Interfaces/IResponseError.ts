import IErrorMessage from './IErrorMessage';

export default interface IResponseError {
  response: {
    data: IErrorMessage
  }
}
