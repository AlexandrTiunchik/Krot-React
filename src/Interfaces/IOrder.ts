export interface IOrderDish {
  dishId: string,
  count: number
}

export interface IOrder {
  userId: string;
  dishes: IOrderDish[],
  status: string;
}
