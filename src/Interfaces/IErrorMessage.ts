export default interface IErrorMessage {
  msg?: string;
  value?: any;
  param: string;
  location?: string;
}
