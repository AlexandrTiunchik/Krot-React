export default interface IDish {
  id: string;
  title: string;
  image: string;
  description: string;
  date: string;
  price: string;
  ingredients: any;
  category: string;
}
